Adaptive Huffman CoDec
========================

The classic Huffman entropy encoder and decoder.


Description
-----------

I developed this program for a college homework. This program is able to compress and decompress any file using [Huffman encoding][1].


Installation
------------

1. Clone the source code
> `git clone https://gitlab.com/pedrogarcia/adaptative-huffman`
2. Build the source
> `cd src`  
> `make`


Execution
---------

To compress the **raw.txt** file, saving as the compressed file **coded.huff**, proceed as follows

> `./huff e raw.txt coded.huff`

To recover/decompress the original data in **raw.txt** file, use the `d` option

> `./huff d coded.huff raw_recovered.txt`


License
-------

This project is released under [GNU GPL version 2][2].


[1]: http://en.wikipedia.org/wiki/Huffman_coding
[2]: http://www.gnu.org/licenses/gpl-2.0.html
