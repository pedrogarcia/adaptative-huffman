/*
 * HuffmanDecoder.h
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */

#ifndef HUFFMANDECODER_H_
#define HUFFMANDECODER_H_

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ios;
using std::ios_base;
using std::ifstream;
using std::ofstream;

#include <fstream>
using std::fstream;

#include "AdaptativeHuffman.h"
#include "sharedconfig.h"
#include "Node.h"

class HuffmanDecoder: public AdaptativeHuffman
{
    public:
        HuffmanDecoder(ifstream&, ofstream&);
        virtual ~HuffmanDecoder();
    private:
        Node *nodeOrderListTail;

        void decode(ifstream&, ofstream&);
        int32_t getASymbol(int32_t&);
        int32_t readBitBuffer();
        int32_t readNumberOfSymbols(void);
        int32_t restoreSymbol(Node*, int32_t*);
        void startVariables(void);
        void writeSymbol(int32_t&);
};

#endif /* HUFFMANDECODER_H_ */
