//============================================================================
// Name        : Main.cpp
// Author      : Pedro Garcia
// Version     :
// Copyright   : 2010 Pedro Garcia
// Description : Huffman encoder and decoder
//============================================================================

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ios;
using std::ifstream;
using std::ofstream;

#include <fstream>
using std::fstream;

#include <cstring>
using std::string;

#include <cstdlib>

#include "HuffmanEncoder.h"
#include "HuffmanDecoder.h"

void howto(char *input)
{
    cerr << "Usage: " << input << " <operation> inputfile outputfile\n";
    cerr << "Possible operations: \n";
    cerr << "\t e -> encode \n\t d -> decode" << endl;
}

int main(int argc, char **argv)
{
    ifstream fin;
    ofstream fout;
    AdaptativeHuffman *huff;

    if (argc != 4) {
        howto(argv[0]);
        exit(EXIT_FAILURE);
    }

    fin.open(argv[2], ios::in | ios::binary);
    fout.open(argv[3], ios::out | ios::binary);

    if (!fin | !fout) {
        howto(argv[0]);
        return EXIT_FAILURE;
    }

    if (*argv[1] == 'd') {
        cout << "Decoding..." << endl;
        huff = new HuffmanDecoder(fin, fout);
        delete huff;
    } else {
        cout << "Encoding..." << endl;
        huff = new HuffmanEncoder(fin, fout);
        delete huff;
    }

    cout << "Done" << endl;
    return EXIT_SUCCESS;
}
