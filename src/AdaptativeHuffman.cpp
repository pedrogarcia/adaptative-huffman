/*
 * AdaptativeHuffman.cpp
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */

#include <cstdlib>
#include <cstdio>
#include "AdaptativeHuffman.h"
#include "sharedconfig.h"
#include "Node.h"

AdaptativeHuffman::AdaptativeHuffman()
{
}

AdaptativeHuffman::~AdaptativeHuffman()
{
}

void AdaptativeHuffman::updateProcedure(int32_t symbol)
{
    Node* newNYT;
    Node* externalNode;

    if (isFirstAppearanceforSymbol(symbol)) {
        newNYT = new Node();
        externalNode = new Node();
        givesBirthToNewNYTandExternalNode(symbol, externalNode, newNYT);
        incrementNodeWeight(externalNode->parent);
    } else {
        incrementNodeWeight(listOfSymbols[symbol]);
    }
}

void AdaptativeHuffman::eraseParentNode(Node** parentNode)
{
    *parentNode = (Node *) listOfParents;
    listOfParents = parentNode;
}
bool AdaptativeHuffman::isFirstAppearanceforSymbol(int32_t symbol)
{
    return (listOfSymbols[symbol] == NULL);
}

Node** AdaptativeHuffman::getParentNode(void)
{
    if (listOfParents == NULL)
        return ((Node **) (new Node*));
    else {
        Node** parentNode = listOfParents;
        listOfParents = (Node **) *parentNode;
        return parentNode;
    }
}

void AdaptativeHuffman::givesBirthToNewNYTandExternalNode(int32_t symbol,
                                                          Node *externalNode,
                                                          Node *newNYT)
{
    externalNode->symbol = INTERNAL_NODE;
    externalNode->weight = ONE;
    externalNode->next = headOflistOfSymbols->next;
    if (headOflistOfSymbols->next != LEAF) {
        headOflistOfSymbols->next->previous = externalNode;
        if (headOflistOfSymbols->next->weight == ONE) {
            externalNode->root = headOflistOfSymbols->next->root;
        } else {
            externalNode->root = getParentNode();
            *externalNode->root = externalNode;
        }
    } else {
        externalNode->root = getParentNode();
        *externalNode->root = externalNode;
    }
    headOflistOfSymbols->next = externalNode;
    externalNode->previous = headOflistOfSymbols;

    newNYT->symbol = symbol;
    newNYT->weight = ONE;
    newNYT->next = headOflistOfSymbols->next;
    if (headOflistOfSymbols->next != LEAF) {
        headOflistOfSymbols->next->previous = newNYT;
        if (headOflistOfSymbols->next->weight == ONE) {
            newNYT->root = headOflistOfSymbols->next->root;
        } else {
            newNYT->root = getParentNode();
            *newNYT->root = externalNode;
        }
    } else {
        newNYT->root = getParentNode();
        *newNYT->root = newNYT;
    }
    headOflistOfSymbols->next = newNYT;
    newNYT->previous = headOflistOfSymbols;
    newNYT->left = newNYT->right = NULL;

    if (headOflistOfSymbols->parent != NULL) {
        if (headOflistOfSymbols->parent->left == headOflistOfSymbols) {
            headOflistOfSymbols->parent->left = externalNode;
        } else {
            headOflistOfSymbols->parent->right = externalNode;
        }
    } else {
        tree = externalNode;
    }

    externalNode->right = newNYT;
    externalNode->left = headOflistOfSymbols;
    externalNode->parent = headOflistOfSymbols->parent;
    headOflistOfSymbols->parent = externalNode;
    newNYT->parent = externalNode;
    listOfSymbols[symbol] = newNYT;
}

void AdaptativeHuffman::incrementNodeWeight(Node* node)
{
    Node* externalNode;

    if (node == LEAF)
        return;

    if ((node->next != LEAF) && (node->next->weight == node->weight)) {
        externalNode = *node->root;
        if (externalNode != node->parent)
            swapVertical(externalNode, node);
        swapHorizontal(externalNode, node);
    }
    if (node->previous && node->previous->weight == node->weight) {
        *node->root = node->previous;
    } else {
        *node->root = NULL;
        eraseParentNode(node->root);
    }
    node->weight++;
    if (node->next && node->next->weight == node->weight) {
        node->root = node->next->root;
    } else {
        node->root = getParentNode();
        *node->root = node;
    }
    if (node->parent != LEAF) {
        incrementNodeWeight(node->parent);
        if (node->previous == node->parent) {
            swapHorizontal(node, node->parent);
            if (*node->root == node)
                *node->root = node->parent;
        }
    }
}

void AdaptativeHuffman::swapHorizontal(Node* node1, Node* node2)
{
    Node* aux;

    aux = node1->next;
    node1->next = node2->next;
    node2->next = aux;

    aux = node1->previous;
    node1->previous = node2->previous;
    node2->previous = aux;

    if (node1->next == node1)
        node1->next = node2;
    if (node2->next == node2)
        node2->next = node1;

    if (node1->next != LEAF)
        node1->next->previous = node1;
    if (node2->next != LEAF)
        node2->next->previous = node2;
    if (node1->previous != LEAF)
        node1->previous->next = node1;
    if (node2->previous != LEAF)
        node2->previous->next = node2;
}

void AdaptativeHuffman::swapVertical(Node* node1, Node* node2)
{
    Node* parent1;
    Node* parent2;

    parent1 = node1->parent;
    parent2 = node2->parent;

    if (parent1 != LEAF) {
        if (parent1->left == node1) {
            parent1->left = node2;
        } else {
            parent1->right = node2;
        }
    } else {
        tree = node2;
    }

    if (parent2 != LEAF) {
        if (parent2->left == node2) {
            parent2->left = node1;
        } else {
            parent2->right = node1;
        }
    } else {
        tree = node1;
    }
    node1->parent = parent2;
    node2->parent = parent1;
}
