/*
 * AdaptativeHuffman.h
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */

#ifndef ADAPTATIVEHUFFMAN_H_
#define ADAPTATIVEHUFFMAN_H_

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ios;
using std::ios_base;
using std::ifstream;
using std::ofstream;

#include <fstream>
using std::fstream;

#include "Node.h"
#include "sharedconfig.h"

class AdaptativeHuffman
{
    public:
        AdaptativeHuffman();
        virtual ~AdaptativeHuffman();

    protected:
        Node* tree;
        Node* headOflistOfSymbols;
        Node* listOfSymbols[NODE_ORDER_LIST_SIZE];
        Node** listOfParents;
        uint8_t bitBuffer[128];
        int32_t fileBlockSize;
        ifstream* inputFile;
        ofstream* outputFile;

        void updateProcedure(int32_t);
    private:
        void eraseParentNode(Node**);
        bool isFirstAppearanceforSymbol(int32_t);
        Node** getParentNode(void);
        void incrementNodeWeight(Node*);
        void givesBirthToNewNYTandExternalNode(int32_t, Node*, Node*);
        void swapHorizontal(Node*, Node*);
        void swapVertical(Node*, Node*);
};

#endif /* ADAPTATIVEHUFFMAN_H_ */
