/*
 * sharedconfig.h
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */
#define DICIONARY_SIZE 256
#define SYMBOL_BITSIZE 8
#define NYT DICIONARY_SIZE
#define NODE_ORDER_LIST_SIZE DICIONARY_SIZE+1
#define INTERNAL_NODE 0xCAFE
#define LEAF NULL
#define READED_BLOCK_SIZE 1024
#define BUFFER_SIZE 128
#define TRUE 1
#define ONE 1
#define ZERO 0
#define NUMBER_OF_SYMBOLS_BYTESIZE -4
