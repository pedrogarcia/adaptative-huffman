/*
 * Node.cpp
 *
 *  Created on: Apr 30, 2011
 *      Author: pedrogarcia
 */

#include <cstdlib>
#include "Node.h"

Node::Node()
{
    weight = 0;
    symbol = 0;
    left = NULL;
    right = NULL;
    parent = NULL;
    next = NULL;
    previous = NULL;
    root = NULL;
}
