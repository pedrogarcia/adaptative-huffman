/*
 * HuffmanDecoder.cpp
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ios;

#include <fstream>
using std::fstream;

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "HuffmanDecoder.h"
#include "sharedconfig.h"
#include "Node.h"

HuffmanDecoder::HuffmanDecoder(ifstream &input, ofstream &output)
{
    ofstream obuff;

    // first step
    obuff.open(".buffer.buff", ios::out | ios::binary);
    decode(input, obuff);
    obuff.flush();
    obuff.close();

    // second step
    ifstream ibuff;
    ibuff.open(".buffer.buff", ios::in | ios::binary);
    decode(ibuff, output);
    ibuff.close();
    remove(".buffer.buff");
}

HuffmanDecoder::~HuffmanDecoder()
{
}

void HuffmanDecoder::decode(ifstream &input, ofstream &output)
{
    int32_t symbol = ZERO;
    int32_t numberOfSymbols = ZERO;

    inputFile = &input;
    outputFile = &output;
    startVariables();

    numberOfSymbols = readNumberOfSymbols();
    inputFile->seekg(0, ios::beg);

    while (numberOfSymbols--) {
        symbol = getASymbol(symbol);
        writeSymbol(symbol);
        updateProcedure(symbol);
    }
}

int32_t HuffmanDecoder::getASymbol(int32_t &symbol)
{
    restoreSymbol(tree, &symbol);
    if (symbol == NYT) {
        symbol = ZERO;
        for (int32_t i = ZERO; i < SYMBOL_BITSIZE; i++) {
            int32_t step = symbol << ONE;
            symbol = step + readBitBuffer();
        }
    }
    return symbol;
}

int32_t HuffmanDecoder::readBitBuffer()
{
    if (fileBlockSize == READED_BLOCK_SIZE) {
        inputFile->read(reinterpret_cast<char *> (bitBuffer), BUFFER_SIZE
            * sizeof(uint8_t));
        fileBlockSize = 0;
    }
    int32_t shiftSize = 7 - (fileBlockSize % 8);
    int32_t shift = bitBuffer[fileBlockSize >> 3] >> shiftSize;
    int32_t maskShift = shift & TRUE;
    fileBlockSize++;
    return maskShift;
}

int32_t HuffmanDecoder::readNumberOfSymbols(void)
{
    int32_t numberOfSymbols = ZERO;
    uint8_t lastFourBytesOfFile[4];

    if (!inputFile->seekg(NUMBER_OF_SYMBOLS_BYTESIZE, ios::end)) {
        exit(EXIT_FAILURE);
    }

    inputFile->read(reinterpret_cast<char *> (&lastFourBytesOfFile),
                    sizeof(lastFourBytesOfFile) * sizeof(uint8_t));
    for (int32_t bytes = 0; bytes < 4; bytes++) {
        for (int32_t bit = 7; bit >= 0; bit--) {
            int32_t symbols = (lastFourBytesOfFile[bytes] >> bit) & TRUE;
            int32_t step = numberOfSymbols << ONE;
            numberOfSymbols = step + symbols;
        }
    }
    return numberOfSymbols;
}

int32_t HuffmanDecoder::restoreSymbol(Node* node, int32_t* symbol)
{
    while (node && node->symbol == INTERNAL_NODE) {
        int32_t bit = readBitBuffer();
        if (bit == ONE) {
            node = node->right;
        } else {
            node = node->left;
        }
    }
    if (node == NULL) {
        exit(EXIT_FAILURE);
    } else {
      *symbol = node->symbol;
    }
    return node->symbol;
}
void HuffmanDecoder::startVariables(void)
{
    memset(bitBuffer, ZERO, sizeof(bitBuffer));
    memset(listOfSymbols, NULL, sizeof(listOfSymbols));
    tree = new Node();
    tree->weight = ZERO;
    tree->symbol = NYT;
    tree->parent = LEAF;
    tree->left = LEAF;
    tree->right = LEAF;
    tree->next = LEAF;
    tree->previous = LEAF;
    listOfParents = LEAF;
    headOflistOfSymbols = tree;
    listOfSymbols[NYT] = tree;
    nodeOrderListTail = tree;
    fileBlockSize = READED_BLOCK_SIZE;
}

void HuffmanDecoder::writeSymbol(int32_t &symbol)
{
    outputFile->write(reinterpret_cast<char *> (&symbol), sizeof(uint8_t));
}

