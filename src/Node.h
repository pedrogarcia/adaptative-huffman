/*
 * Node.h
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */

#ifndef NODE_H_
#define NODE_H_

#include <stdint.h>

class Node
{
    public:
        uint16_t weight;
        uint16_t symbol;
        Node* left;
        Node* right;
        Node* parent;
        Node* next;
        Node* previous;
        Node** root;
        Node();
};
#endif /* NODE_H_ */
