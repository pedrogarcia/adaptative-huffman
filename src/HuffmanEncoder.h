/*
 * HuffmanEncoder.h
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */

#ifndef HUFFMANENCODER_H_
#define HUFFMANENCODER_H_

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ios;
using std::ios_base;
using std::ifstream;
using std::ofstream;

#include <fstream>
using std::fstream;

#include <cstdio>

#include "sharedconfig.h"
#include "Node.h"
#include "AdaptativeHuffman.h"

class HuffmanEncoder: public AdaptativeHuffman
{
    public:
        HuffmanEncoder(ifstream&, ofstream&);
        virtual ~HuffmanEncoder();
    private:
        void encode(ifstream&, ofstream&);
        void flushBuffer(void);
        bool isTheFirstAppearanceOfSybol(int32_t);
        bool isThisTheLastSymbol(void);
        void sendCodeForNYT(Node*, Node*);
        void sendSymbolToFile(int32_t);
        void startVariables(void);
        void writeBitBuffer(int32_t);
        void writeNumberOfSymbols(int32_t);
};

#endif /* HUFFMANENCODER_H_ */
