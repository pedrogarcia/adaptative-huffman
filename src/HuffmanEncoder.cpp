/*
 * HuffmanEncoder.cpp
 *
 *  Created on: Apr 26, 2011
 *      Author: pedrogarcia
 */

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ios;
using std::ios_base;
using std::ifstream;
using std::ofstream;

#include <fstream>
using std::fstream;

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "HuffmanEncoder.h"
#include "sharedconfig.h"
#include "Node.h"

HuffmanEncoder::HuffmanEncoder(ifstream &input, ofstream &output)
{
    ofstream obuff;
    ifstream ibuff;

    // two-steps Huffman Decoding
    // first step
    obuff.open(".buffer.buff", ios::out | ios::binary);
    encode(input, obuff);
    obuff.flush();
    obuff.close();

    // second step
    ibuff.open(".buffer.buff", ios::in | ios::binary);
    encode(ibuff, output);
    ibuff.close();
    remove(".buffer.buff");
}

HuffmanEncoder::~HuffmanEncoder()
{
}

void HuffmanEncoder::encode(ifstream &input, ofstream &output)
{
    int32_t symbol = ZERO;
    int32_t numberOfSymbols = ZERO;

    inputFile = &input;
    outputFile = &output;
    startVariables();

    while (inputFile->read(reinterpret_cast<char *> (&symbol), sizeof(uint8_t))
        && !isThisTheLastSymbol()) {
        numberOfSymbols++;
        sendSymbolToFile(symbol);
        updateProcedure(symbol);
    }
    writeNumberOfSymbols(numberOfSymbols);
}

void HuffmanEncoder::flushBuffer()
{
    outputFile->write(reinterpret_cast<char *> (bitBuffer), sizeof(uint8_t)
        * ((fileBlockSize + 7) >> 3));
    memset(bitBuffer, 0, sizeof(bitBuffer));
    fileBlockSize = 0;
}

bool HuffmanEncoder::isTheFirstAppearanceOfSybol(int32_t symbol)
{
    return (listOfSymbols[symbol] == NULL);
}

bool HuffmanEncoder::isThisTheLastSymbol(void)
{
    return (inputFile->eof());
}

void HuffmanEncoder::sendCodeForNYT(Node* node, Node* child)
{
    if (node->parent) {
        sendCodeForNYT(node->parent, node);
    }

    if (child != LEAF) {
        if (node->right == child) {
            writeBitBuffer(ONE);
        } else {
            writeBitBuffer(ZERO);
        }
    }
}

void HuffmanEncoder::sendSymbolToFile(int32_t symbol)
{
    if (isTheFirstAppearanceOfSybol(symbol)) {
        sendSymbolToFile(NYT);
        for (int32_t bits = SYMBOL_BITSIZE - 1; bits >= 0; bits--)
            writeBitBuffer((symbol >> bits) & TRUE);
    } else {
        sendCodeForNYT(listOfSymbols[symbol], LEAF);
    }
}
void HuffmanEncoder::startVariables(void)
{
    memset(bitBuffer, ZERO, sizeof(bitBuffer));
    memset(listOfSymbols, NULL, sizeof(listOfSymbols));
    tree = new Node();
    tree->weight = ZERO;
    tree->symbol = NYT;
    tree->parent = LEAF;
    tree->left = LEAF;
    tree->right = LEAF;
    tree->next = LEAF;
    tree->previous = LEAF;
    listOfParents = LEAF;
    headOflistOfSymbols = tree;
    listOfSymbols[NYT] = tree;
    fileBlockSize = ZERO;
}

void HuffmanEncoder::writeBitBuffer(int32_t bit)
{
    if (fileBlockSize == READED_BLOCK_SIZE) {
        outputFile->write(reinterpret_cast<char *> (bitBuffer), BUFFER_SIZE
            * sizeof(uint8_t));
        memset(bitBuffer, ZERO, sizeof(bitBuffer));
        fileBlockSize = ZERO;
    }
    int32_t shiftSize = (7 - (fileBlockSize % 8));
    bitBuffer[fileBlockSize >> 3] |= bit << shiftSize;
    fileBlockSize++;
}

void HuffmanEncoder::writeNumberOfSymbols(int32_t numberOfSymbols)
{
    flushBuffer();
    for (int32_t bits = 31; bits >= 0; bits--)
        writeBitBuffer((numberOfSymbols >> bits) & TRUE);
    flushBuffer();
}

